<?php

namespace App\Console\Commands;

use App\ProcessedCall;
use App\SipPhone;
use Illuminate\Support\Facades\Redis;
use Illuminate\Console\Command;

class RedisSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:subscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis channel';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Redis::subscribe(['dispatch_response'], function($message) {
            echo $message;
//            $processed = json_decode($message);
//            var_dump($message);
//            if ($processed->is_processed) {
//                $userId = SipPhone::select('user_id')->where('number', '=', $processed->sip_number)->first()->user_id;
//
//                foreach ($processed->dispatch->numbers as $id =>$number) {
//                    ProcessedCall::create([
//                        'phone_id' => $id,
//                        'marketing_company_id' => $processed->dispatch->dispatch_id,
//                        'user_id' => $userId,
//                        'status' => 'handled'
//                    ]);
//                }
//            }
        });
    }
}
