<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcessedCall extends Model
{
    protected $fillable = ['phone_id', 'marketing_company_id', 'user_id', 'status'];
}
