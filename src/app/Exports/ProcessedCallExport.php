<?php


namespace App\Exports;


use App\ProcessedCall;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

/**
 * Class ProcessedCallExport
 * @package App\Exports
 */
class ProcessedCallExport implements FromCollection, WithHeadings
{
    use Exportable;

    /**
     * @var $dispatchId
     */
    private $dispatchId;

    public function __construct($dispatchId)
    {
        $this->dispatchId = $dispatchId;
    }

    public function headings(): array
    {
        return [
            'Телефоны',
            'Маркетинговые кампании',
            'Статусы',
            'Пользователи',
            'Дата обработки'
        ];
    }

    /**
     * @return ProcessedCall[]|\Illuminate\Database\Eloquent\Collection|Collection
     */
    public function collection()
    {
        $db = DB::table('processed_calls')
            ->join('phones', 'processed_calls.phone_id', '=', 'phones.id')
            ->join('marketing_companies', 'processed_calls.marketing_company_id', '=', 'marketing_companies.id')
            ->join('users', 'processed_calls.user_id', '=', 'users.id')
            ->join('statuses', 'processed_calls.status_id', '=', 'statuses.id')
            ->select('phones.number', 'marketing_companies.name as mkname', 'statuses.title', 'users.name', 'processed_calls.created_at');

        if ($this->dispatchId) {
            $db->where('processed_calls.marketing_company_id', $this->dispatchId);
        }

        return $db->get();
    }
}