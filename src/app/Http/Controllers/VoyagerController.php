<?php

namespace App\Http\Controllers;

use App\SipPhone;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Redis;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerController as BaseVoyagerController;

class VoyagerController extends BaseVoyagerController
{
    public function index()
    {
        $minutes = 525600; //one year
        $userId = auth()->user()->id;
        $sipNumber = SipPhone::select('number')->where('user_id', $userId)->first()->number;
        Cookie::queue(Cookie::make('sipNumber', $sipNumber, $minutes, null, null, false, false));

        return Voyager::view('voyager::index');
    }

    public function logout()
    {
        $userId = auth()->user()->id;
        $this->unSetSipNumberToUser($userId);
        Cookie::queue(Cookie::forget('sipNumber'));

        app('VoyagerAuth')->logout();

        return redirect()->route('voyager.login');
    }

    private function unSetSipNumberToUser($userId) : void
    {
        Redis::del(SipPhone::select('number')->where('user_id', $userId)->first()->number);
        SipPhone::where('user_id', $userId)->update(['status' => 'left', 'is-logged-in' => 0, 'user_id' =>  null]);
    }
}
