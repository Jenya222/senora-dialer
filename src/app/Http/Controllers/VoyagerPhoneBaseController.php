<?php


namespace App\Http\Controllers;


use App\Imports\PhonesImport;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class VoyagerPhoneBaseController extends VoyagerBaseController
{
    /**
     * Store PhoneBase with uploaded file
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        $phoneBaseSavedId = $data->getAttribute('id');
        $this->savePhoneBase($request, $phoneBaseSavedId);

        event(new BreadDataAdded($dataType, $data));

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    /**
     * @param Request $request
     * @param $phoneBaseSavedId
     */
    private function savePhoneBase(Request $request, $phoneBaseSavedId) : void
    {
        $file = $request->file('phoneBase');
        $uploadedPhoneBase = $file->storeAs('phoneBase', $file->getClientOriginalName());

        Excel::import(new PhonesImport($phoneBaseSavedId), $uploadedPhoneBase);
        Storage::delete($uploadedPhoneBase);
    }
}