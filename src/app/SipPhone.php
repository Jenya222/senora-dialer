<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SipPhone extends Model
{
    protected $fillable = ['number', 'is-logged-in', 'user_id', 'status'];
}
