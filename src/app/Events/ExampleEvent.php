<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ExampleEvent implements ShouldBroadcast
{
    use SerializesModels;

    public $username;

    public function __construct($username)
    {
        $this->username = $username;
    }

    public function broadcastOn()
    {
        return new PresenceChannel('test-channel');
    }

    //Транслировать короткое имя события без полного namespace path
    public function broadcastAs()
    {
        return ['ExampleEvent'];
    }
}
