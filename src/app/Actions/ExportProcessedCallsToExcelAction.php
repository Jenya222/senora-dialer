<?php


namespace App\Actions;


use TCG\Voyager\Actions\AbstractAction;

class ExportProcessedCallsToExcelAction extends AbstractAction
{
    /**
     * @return mixed
     */
    public function getTitle()
    {
        return 'Скачать результат';
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return 'voyager-download';
    }

    /**
     * @return mixed
     */
    public function getDefaultRoute()
    {
        $fieldName = $this->data->name;
        $fieldCreatedAt = $this->data->created_at;

        return route('marketing.company.export', [$this->data->{$this->data->getKeyName()}, $fieldName. '_' . $fieldCreatedAt]);
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right',
        ];
    }

    /**
     * @return bool
     */
    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'marketing-companies';
    }
}