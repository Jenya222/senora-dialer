<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoipServer extends Model
{
    protected $fillable = ['name', 'url', 'status'];
}
