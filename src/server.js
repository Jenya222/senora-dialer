var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var redis = require("redis");
var sub = redis.createClient("redis://senora-redis:6379"), pub = redis.createClient("redis://senora-redis:6379");
var client = redis.createClient("redis://senora-redis:6379");

sub.on("subscribe", function (channel, count) {


});

sub.on("message", function (channel, message) {
    let parsedMeassge = JSON.parse(message);

    let phone = parsedMeassge.number;
    let sipNumber = parsedMeassge.sip_number;

    client.get(phone, function(err, result){
        //Do some processing with the value from this field and watch it after
        if (err) {
            console.log(err);
            return false;
        }

        let parsedResult = JSON.parse(result);

        if (!parsedResult) {
            return false;
        }

        if (!parsedResult.hasOwnProperty('id')) {
            return false;
        }

        parsedMeassge.number_id = parsedResult['id'];
        parsedMeassge.dispatch_id = parsedResult['dispatch_id'];
        client.del(phone);

        io.emit('user_' + sipNumber, JSON.stringify(parsedMeassge));
    });
});

sub.subscribe("dispatch_response");

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true }));// for parsing application/x-www-form-urlencoded

app.post('/api/dispatch_response', (req, res) => {
    let body = req.body;

    if (body.is_processed) {
        pub.publish('dispatch_response', JSON.stringify(req.body));
    }


    console.log(JSON.stringify(body));
    return res.sendStatus(200);
});

//Выбираем номер порта
http.listen(3000, function(){
    console.log('listening on *:3000');

});
