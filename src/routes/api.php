<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::group(['middleware' => 'api'], function(){
    //Fetch Contacts
//    Route::get('dispatch_respones', function(){
//        return Contact::latest()->orderBy('created_at', 'desc')-> get();
//    });

    //Get Single Contact
//    Route::get('contact/{id}', function($id){
//        return Contact::findOrFail($id);
//    });

    //Add Contact
//    Route::post('dispatch_response', function(Request $request){
//        $bodyContent = $request->getContent();
//
//        return Redis::publish('dispatch_response', $bodyContent);
//    });

    //Update Contact
//    Route::patch('contact/{id}', function(Request $request, $id){
//        Contact::findOrFail($id)->update(
//            [
//                'name' => $request->input(['name']),
//                'email' => $request->input(['email']),
//                'phone' => $request->input(['phone'])
//            ]
//        );
//    });

    //Delete Contact
//    Route::delete('contact/{id}', function($id){
//        return Contact::destroy($id);
//    });
//});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
