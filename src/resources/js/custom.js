import Vue from 'vue'
import VueCookies from 'vue-cookies'
Vue.use(VueCookies);
window.io = require('socket.io-client');

let sipNumber = VueCookies.get('sipNumber');

let hostname = window.location.hostname;

var socket = io(hostname + ':3000');

socket.on('user_' + sipNumber, function(msg){
    let response = JSON.parse(msg);
    let url = window.location.origin + "/processed-calls/create?" + "numberId=" + response.number_id + "&dispatchId=" + response.dispatch_id;
    window.open(url);
});
