@extends('voyager::bread.edit-add')
@section('css')
    @parent
    <style>
        .panel-footer {padding: 10px 30px}
        .custom-file {margin-bottom: 20px}
    </style>
@endsection
@section('submit-buttons')
    <div class="input-group">
        <p>Шаблон загружаемого файла <a href="{{ asset('phonebase.csv') }}">СКАЧАТЬ</a></p>
        <div class="custom-file">
            <input name="phoneBase" type="file" class="custom-file-input">
        </div>
    </div>
    @parent
@endsection