<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneBaseToMarketingCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone_base_to_marketing_company', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('phone_base_id');
            $table->bigInteger('marketing_company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phone_base_to_marketing_company');
    }
}
