<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusIdToProcessedCall extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('processed_calls', function (Blueprint $table) {
            $table->integer('status_id')->nullable()->comment('status of a call');
            $table->dropColumn('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('processed_calls', function (Blueprint $table) {
            $table->dropColumn('status_id');
            $table->string('status')->comment('status of this call');
        });
    }
}
