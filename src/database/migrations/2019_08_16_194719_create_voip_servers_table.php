<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoipServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voip_servers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('server name');
            $table->string('url')->comment('server address');
            $table->string('token')->comment('token to connect');
            $table->string('status')->comment('status of using');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voip_servers');
    }
}
