<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSipPhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sip_phones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('number')->comment('sip phone number');
            $table->string('status')->comment('set status: Work, Wait, Offline');
            $table->boolean('is-logged-in')->comment('check whether this number logged in');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sip_phones');
    }
}
