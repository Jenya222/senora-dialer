<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessedCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processed_calls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('phone_id')->comment('relation to phone number id');
            $table->integer('marketing_company_id')->comment('relation to marketing company id');
            $table->integer('user_id')->comment('relation to user id which handle this call');
            $table->string('status')->comment('status of this call');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processed_calls');
    }
}
