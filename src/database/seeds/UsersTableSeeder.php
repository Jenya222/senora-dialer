<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$C.2x6FL6oH8WbGPUacAsp.i9Cyx2iGL/1oolMrexfus6PY5P5L8YC',
                'remember_token' => NULL,
                'settings' => '{"locale":"ru"}',
                'created_at' => '2019-08-17 20:29:46',
                'updated_at' => '2019-09-09 22:32:19',
            ),
            1 => 
            array (
                'id' => 3,
                'role_id' => 2,
                'name' => 'User',
                'email' => 'user@user.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$/7OmyeySbz8PsrI12DSWje5PzqFgl.0TNkV7KDcwl7Ty8kpgMFZUa',
                'remember_token' => NULL,
                'settings' => '{"locale":"ru"}',
                'created_at' => '2019-08-28 18:19:20',
                'updated_at' => '2019-08-28 18:19:20',
            ),
            2 => 
            array (
                'id' => 4,
                'role_id' => 3,
                'name' => 'manager',
                'email' => 'manager@manager.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$1Dj56SbJLn0OkQqh4ibaHOE0tAeq4RvsScjfm8rPU.4f8QSuL9ziy',
                'remember_token' => NULL,
                'settings' => '{"locale":"ru"}',
                'created_at' => '2019-09-02 16:52:41',
                'updated_at' => '2019-09-02 16:52:41',
            ),
            3 => 
            array (
                'id' => 5,
                'role_id' => 2,
                'name' => 'test',
                'email' => NULL,
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$I4ZstV4AjBO3IT1l9hNPte7tWe01JtDBwWejFX6xBX8KSz4E9RQSm',
                'remember_token' => NULL,
                'settings' => '{"locale":"ru"}',
                'created_at' => '2019-09-03 22:47:17',
                'updated_at' => '2019-09-03 22:47:17',
            ),
        ));
        
        
    }
}