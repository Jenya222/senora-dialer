<?php

use Illuminate\Database\Seeder;

class VoipServersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('voip_servers')->delete();
        
        \DB::table('voip_servers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'mytest',
                'url' => 'http://localhost:3000/api/dispatch_response',
                'status' => 'ok',
                'created_at' => '2019-08-28 18:00:02',
                'updated_at' => '2019-08-28 18:00:02',
            ),
        ));
        
        
    }
}