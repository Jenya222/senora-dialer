<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_hooks',
                'table_name' => NULL,
                'created_at' => '2019-08-17 20:31:39',
                'updated_at' => '2019-08-17 20:31:39',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'browse_marketing_companies',
                'table_name' => 'marketing_companies',
                'created_at' => '2019-08-17 20:32:54',
                'updated_at' => '2019-08-17 20:32:54',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'read_marketing_companies',
                'table_name' => 'marketing_companies',
                'created_at' => '2019-08-17 20:32:54',
                'updated_at' => '2019-08-17 20:32:54',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'edit_marketing_companies',
                'table_name' => 'marketing_companies',
                'created_at' => '2019-08-17 20:32:54',
                'updated_at' => '2019-08-17 20:32:54',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'add_marketing_companies',
                'table_name' => 'marketing_companies',
                'created_at' => '2019-08-17 20:32:54',
                'updated_at' => '2019-08-17 20:32:54',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'delete_marketing_companies',
                'table_name' => 'marketing_companies',
                'created_at' => '2019-08-17 20:32:54',
                'updated_at' => '2019-08-17 20:32:54',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'browse_phone_bases',
                'table_name' => 'phone_bases',
                'created_at' => '2019-08-17 20:33:13',
                'updated_at' => '2019-08-17 20:33:13',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'read_phone_bases',
                'table_name' => 'phone_bases',
                'created_at' => '2019-08-17 20:33:13',
                'updated_at' => '2019-08-17 20:33:13',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'edit_phone_bases',
                'table_name' => 'phone_bases',
                'created_at' => '2019-08-17 20:33:13',
                'updated_at' => '2019-08-17 20:33:13',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'add_phone_bases',
                'table_name' => 'phone_bases',
                'created_at' => '2019-08-17 20:33:14',
                'updated_at' => '2019-08-17 20:33:14',
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'delete_phone_bases',
                'table_name' => 'phone_bases',
                'created_at' => '2019-08-17 20:33:14',
                'updated_at' => '2019-08-17 20:33:14',
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'browse_phones',
                'table_name' => 'phones',
                'created_at' => '2019-08-17 21:30:00',
                'updated_at' => '2019-08-17 21:30:00',
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'read_phones',
                'table_name' => 'phones',
                'created_at' => '2019-08-17 21:30:00',
                'updated_at' => '2019-08-17 21:30:00',
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'edit_phones',
                'table_name' => 'phones',
                'created_at' => '2019-08-17 21:30:00',
                'updated_at' => '2019-08-17 21:30:00',
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'add_phones',
                'table_name' => 'phones',
                'created_at' => '2019-08-17 21:30:00',
                'updated_at' => '2019-08-17 21:30:00',
            ),
            40 => 
            array (
                'id' => 41,
                'key' => 'delete_phones',
                'table_name' => 'phones',
                'created_at' => '2019-08-17 21:30:00',
                'updated_at' => '2019-08-17 21:30:00',
            ),
            41 => 
            array (
                'id' => 42,
                'key' => 'browse_sip_phones',
                'table_name' => 'sip_phones',
                'created_at' => '2019-08-17 21:33:59',
                'updated_at' => '2019-08-17 21:33:59',
            ),
            42 => 
            array (
                'id' => 43,
                'key' => 'read_sip_phones',
                'table_name' => 'sip_phones',
                'created_at' => '2019-08-17 21:33:59',
                'updated_at' => '2019-08-17 21:33:59',
            ),
            43 => 
            array (
                'id' => 44,
                'key' => 'edit_sip_phones',
                'table_name' => 'sip_phones',
                'created_at' => '2019-08-17 21:33:59',
                'updated_at' => '2019-08-17 21:33:59',
            ),
            44 => 
            array (
                'id' => 45,
                'key' => 'add_sip_phones',
                'table_name' => 'sip_phones',
                'created_at' => '2019-08-17 21:33:59',
                'updated_at' => '2019-08-17 21:33:59',
            ),
            45 => 
            array (
                'id' => 46,
                'key' => 'delete_sip_phones',
                'table_name' => 'sip_phones',
                'created_at' => '2019-08-17 21:33:59',
                'updated_at' => '2019-08-17 21:33:59',
            ),
            46 => 
            array (
                'id' => 47,
                'key' => 'browse_voip_servers',
                'table_name' => 'voip_servers',
                'created_at' => '2019-08-17 21:34:23',
                'updated_at' => '2019-08-17 21:34:23',
            ),
            47 => 
            array (
                'id' => 48,
                'key' => 'read_voip_servers',
                'table_name' => 'voip_servers',
                'created_at' => '2019-08-17 21:34:23',
                'updated_at' => '2019-08-17 21:34:23',
            ),
            48 => 
            array (
                'id' => 49,
                'key' => 'edit_voip_servers',
                'table_name' => 'voip_servers',
                'created_at' => '2019-08-17 21:34:23',
                'updated_at' => '2019-08-17 21:34:23',
            ),
            49 => 
            array (
                'id' => 50,
                'key' => 'add_voip_servers',
                'table_name' => 'voip_servers',
                'created_at' => '2019-08-17 21:34:23',
                'updated_at' => '2019-08-17 21:34:23',
            ),
            50 => 
            array (
                'id' => 51,
                'key' => 'delete_voip_servers',
                'table_name' => 'voip_servers',
                'created_at' => '2019-08-17 21:34:23',
                'updated_at' => '2019-08-17 21:34:23',
            ),
            51 => 
            array (
                'id' => 52,
                'key' => 'browse_processed_calls',
                'table_name' => 'processed_calls',
                'created_at' => '2019-08-17 21:34:48',
                'updated_at' => '2019-08-17 21:34:48',
            ),
            52 => 
            array (
                'id' => 53,
                'key' => 'read_processed_calls',
                'table_name' => 'processed_calls',
                'created_at' => '2019-08-17 21:34:48',
                'updated_at' => '2019-08-17 21:34:48',
            ),
            53 => 
            array (
                'id' => 54,
                'key' => 'edit_processed_calls',
                'table_name' => 'processed_calls',
                'created_at' => '2019-08-17 21:34:48',
                'updated_at' => '2019-08-17 21:34:48',
            ),
            54 => 
            array (
                'id' => 55,
                'key' => 'add_processed_calls',
                'table_name' => 'processed_calls',
                'created_at' => '2019-08-17 21:34:48',
                'updated_at' => '2019-08-17 21:34:48',
            ),
            55 => 
            array (
                'id' => 56,
                'key' => 'delete_processed_calls',
                'table_name' => 'processed_calls',
                'created_at' => '2019-08-17 21:34:48',
                'updated_at' => '2019-08-17 21:34:48',
            ),
            56 => 
            array (
                'id' => 57,
                'key' => 'browse_statuses',
                'table_name' => 'statuses',
                'created_at' => '2019-09-04 14:56:37',
                'updated_at' => '2019-09-04 14:56:37',
            ),
            57 => 
            array (
                'id' => 58,
                'key' => 'read_statuses',
                'table_name' => 'statuses',
                'created_at' => '2019-09-04 14:56:37',
                'updated_at' => '2019-09-04 14:56:37',
            ),
            58 => 
            array (
                'id' => 59,
                'key' => 'edit_statuses',
                'table_name' => 'statuses',
                'created_at' => '2019-09-04 14:56:37',
                'updated_at' => '2019-09-04 14:56:37',
            ),
            59 => 
            array (
                'id' => 60,
                'key' => 'add_statuses',
                'table_name' => 'statuses',
                'created_at' => '2019-09-04 14:56:37',
                'updated_at' => '2019-09-04 14:56:37',
            ),
            60 => 
            array (
                'id' => 61,
                'key' => 'delete_statuses',
                'table_name' => 'statuses',
                'created_at' => '2019-09-04 14:56:37',
                'updated_at' => '2019-09-04 14:56:37',
            ),
        ));
        
        
    }
}