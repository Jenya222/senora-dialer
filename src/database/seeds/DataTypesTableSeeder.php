<?php

use Illuminate\Database\Seeder;

class DataTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('data_types')->delete();
        
        \DB::table('data_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'users',
                'slug' => 'users',
                'display_name_singular' => 'Пользователь',
                'display_name_plural' => 'Пользователи',
                'icon' => 'voyager-person',
                'model_name' => 'TCG\\Voyager\\Models\\User',
                'policy_name' => 'TCG\\Voyager\\Policies\\UserPolicy',
                'controller' => 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}',
                'created_at' => '2019-08-17 20:31:36',
                'updated_at' => '2019-09-03 22:44:21',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'menus',
                'slug' => 'menus',
                'display_name_singular' => 'Menu',
                'display_name_plural' => 'Menus',
                'icon' => 'voyager-list',
                'model_name' => 'TCG\\Voyager\\Models\\Menu',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-08-17 20:31:37',
                'updated_at' => '2019-08-17 20:31:37',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'roles',
                'slug' => 'roles',
                'display_name_singular' => 'Role',
                'display_name_plural' => 'Roles',
                'icon' => 'voyager-lock',
                'model_name' => 'TCG\\Voyager\\Models\\Role',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-08-17 20:31:37',
                'updated_at' => '2019-08-17 20:31:37',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'marketing_companies',
                'slug' => 'marketing-companies',
                'display_name_singular' => 'Маркетинговая компания',
                'display_name_plural' => 'Маркетинговые компании',
                'icon' => NULL,
                'model_name' => 'App\\MarketingCompany',
                'policy_name' => NULL,
                'controller' => '\\App\\Http\\Controllers\\VoyagerMarketingCompanyController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2019-08-17 20:32:53',
                'updated_at' => '2019-08-28 12:13:29',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'phone_bases',
                'slug' => 'phone-bases',
                'display_name_singular' => 'База телефонов',
                'display_name_plural' => 'Базы телефонов',
                'icon' => NULL,
                'model_name' => 'App\\PhoneBase',
                'policy_name' => NULL,
                'controller' => '\\App\\Http\\Controllers\\VoyagerPhoneBaseController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2019-08-17 20:33:13',
                'updated_at' => '2019-09-02 16:40:52',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'phones',
                'slug' => 'phones',
                'display_name_singular' => 'Телефон',
                'display_name_plural' => 'Телефоны',
                'icon' => 'voyager-telephone',
                'model_name' => 'App\\Phone',
                'policy_name' => NULL,
                'controller' => '\\App\\Http\\Controllers\\VoyagerPhoneController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2019-08-17 21:29:59',
                'updated_at' => '2019-09-06 15:04:09',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'sip_phones',
                'slug' => 'sip-phones',
                'display_name_singular' => 'Sip номер',
                'display_name_plural' => 'Sip номера',
                'icon' => NULL,
                'model_name' => 'App\\SipPhone',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2019-08-17 21:33:59',
                'updated_at' => '2019-09-02 16:37:51',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'voip_servers',
                'slug' => 'voip-servers',
                'display_name_singular' => 'Voip Server',
                'display_name_plural' => 'Voip Servers',
                'icon' => NULL,
                'model_name' => 'App\\VoipServer',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2019-08-17 21:34:22',
                'updated_at' => '2019-08-28 17:59:33',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'processed_calls',
                'slug' => 'processed-calls',
                'display_name_singular' => 'Обработанный вызов',
                'display_name_plural' => 'Обработанные вызовы',
                'icon' => NULL,
                'model_name' => 'App\\ProcessedCall',
                'policy_name' => NULL,
                'controller' => '\\App\\Http\\Controllers\\VoyagerProcessedCallController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2019-08-17 21:34:48',
                'updated_at' => '2019-09-04 15:04:16',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'statuses',
                'slug' => 'statuses',
                'display_name_singular' => 'Статус',
                'display_name_plural' => 'Статусы',
                'icon' => 'voyager-puzzle',
                'model_name' => 'App\\Status',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                'created_at' => '2019-09-04 14:56:37',
                'updated_at' => '2019-09-04 14:59:16',
            ),
        ));
        
        
    }
}