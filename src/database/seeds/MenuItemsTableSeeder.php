<?php

use Illuminate\Database\Seeder;

class MenuItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menu_items')->delete();
        
        \DB::table('menu_items')->insert(array (
            0 => 
            array (
                'id' => 3,
                'menu_id' => 1,
                'title' => 'Пользователи',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-person',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 3,
                'created_at' => '2019-08-17 20:31:37',
                'updated_at' => '2019-09-02 17:29:37',
                'route' => 'voyager.users.index',
                'parameters' => 'null',
            ),
            1 => 
            array (
                'id' => 4,
                'menu_id' => 1,
                'title' => 'Роли',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-lock',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 2,
                'created_at' => '2019-08-17 20:31:37',
                'updated_at' => '2019-09-02 17:29:59',
                'route' => 'voyager.roles.index',
                'parameters' => 'null',
            ),
            2 => 
            array (
                'id' => 5,
                'menu_id' => 1,
                'title' => 'Tools',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-tools',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 9,
                'created_at' => '2019-08-17 20:31:37',
                'updated_at' => '2019-08-17 20:31:37',
                'route' => NULL,
                'parameters' => NULL,
            ),
            3 => 
            array (
                'id' => 6,
                'menu_id' => 1,
                'title' => 'Menu Builder',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-list',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 10,
                'created_at' => '2019-08-17 20:31:37',
                'updated_at' => '2019-08-17 20:31:37',
                'route' => 'voyager.menus.index',
                'parameters' => NULL,
            ),
            4 => 
            array (
                'id' => 7,
                'menu_id' => 1,
                'title' => 'Database',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-data',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 11,
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
                'route' => 'voyager.database.index',
                'parameters' => NULL,
            ),
            5 => 
            array (
                'id' => 8,
                'menu_id' => 1,
                'title' => 'Compass',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-compass',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 12,
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
                'route' => 'voyager.compass.index',
                'parameters' => NULL,
            ),
            6 => 
            array (
                'id' => 9,
                'menu_id' => 1,
                'title' => 'BREAD',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-bread',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 13,
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
                'route' => 'voyager.bread.index',
                'parameters' => NULL,
            ),
            7 => 
            array (
                'id' => 10,
                'menu_id' => 1,
                'title' => 'Settings',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-settings',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 14,
                'created_at' => '2019-08-17 20:31:38',
                'updated_at' => '2019-08-17 20:31:38',
                'route' => 'voyager.settings.index',
                'parameters' => NULL,
            ),
            8 => 
            array (
                'id' => 11,
                'menu_id' => 1,
                'title' => 'Hooks',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-hook',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 13,
                'created_at' => '2019-08-17 20:31:39',
                'updated_at' => '2019-08-17 20:31:39',
                'route' => 'voyager.hooks',
                'parameters' => NULL,
            ),
            9 => 
            array (
                'id' => 12,
                'menu_id' => 1,
                'title' => 'Маркетинговые компании',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-news',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 15,
                'created_at' => '2019-08-17 20:32:54',
                'updated_at' => '2019-08-28 12:15:16',
                'route' => 'voyager.marketing-companies.index',
                'parameters' => 'null',
            ),
            10 => 
            array (
                'id' => 13,
                'menu_id' => 1,
                'title' => 'Базы телефонов',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-window-list',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 16,
                'created_at' => '2019-08-17 20:33:14',
                'updated_at' => '2019-08-28 12:19:58',
                'route' => 'voyager.phone-bases.index',
                'parameters' => 'null',
            ),
            11 => 
            array (
                'id' => 14,
                'menu_id' => 1,
                'title' => 'Телефоны',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-telephone',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 17,
                'created_at' => '2019-08-17 21:30:00',
                'updated_at' => '2019-09-02 16:35:04',
                'route' => 'voyager.phones.index',
                'parameters' => 'null',
            ),
            12 => 
            array (
                'id' => 15,
                'menu_id' => 1,
                'title' => 'Sip Номера',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-phone',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 18,
                'created_at' => '2019-08-17 21:33:59',
                'updated_at' => '2019-08-28 12:22:10',
                'route' => 'voyager.sip-phones.index',
                'parameters' => 'null',
            ),
            13 => 
            array (
                'id' => 16,
                'menu_id' => 1,
                'title' => 'Voip Servers',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-browser',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 19,
                'created_at' => '2019-08-17 21:34:23',
                'updated_at' => '2019-08-28 17:49:31',
                'route' => 'voyager.voip-servers.index',
                'parameters' => 'null',
            ),
            14 => 
            array (
                'id' => 17,
                'menu_id' => 1,
                'title' => 'Обработанные вызовы',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-check-circle',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 20,
                'created_at' => '2019-08-17 21:34:49',
                'updated_at' => '2019-08-28 12:23:00',
                'route' => 'voyager.processed-calls.index',
                'parameters' => 'null',
            ),
            15 => 
            array (
                'id' => 18,
                'menu_id' => 1,
                'title' => 'Статусы',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-puzzle',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 21,
                'created_at' => '2019-09-04 14:56:38',
                'updated_at' => '2019-09-04 15:04:58',
                'route' => 'voyager.statuses.index',
                'parameters' => 'null',
            ),
        ));
        
        
    }
}