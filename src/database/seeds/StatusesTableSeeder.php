<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('statuses')->delete();
        
        \DB::table('statuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'ПЕРЕЗВОНИТЬ',
                'created_at' => '2019-09-04 15:06:47',
                'updated_at' => '2019-09-04 15:06:47',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'ОТКАЗ',
                'created_at' => '2019-09-04 15:07:11',
                'updated_at' => '2019-09-04 15:07:11',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'ОФОРМЛЕН',
                'created_at' => '2019-09-04 15:07:48',
                'updated_at' => '2019-09-04 15:07:48',
            ),
        ));
        
        
    }
}